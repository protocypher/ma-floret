# Floret

**Flower Shop**

Floret is a basic, text based, floral delivery management application that uses
the `cmd.Cmd` framework. It manages when and where orders are due as well as by
which time the flowers for those order should be started and completed.
Different flowers have differing durations of freshness and so boquets should
be started at differing times before being sent out to maximize freshnes.

