from setuptools import setup


setup(
      name="Floret",
      version="0.1.0",
      packages=["floret"],
      url="https://bitbucket.org/protocypher/ma-floret",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A flower boquet delivery application."
)

